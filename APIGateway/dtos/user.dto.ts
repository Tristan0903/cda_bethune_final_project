export interface UserRequestDto{
    email : string,
    password : string
}

export interface UserResponseDto extends UserRequestDto{
    id : number

}