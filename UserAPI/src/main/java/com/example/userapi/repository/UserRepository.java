package com.example.userapi.repository;

import com.example.userapi.entities.User;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface UserRepository extends CrudRepository<User, Integer> {

    public  User findByRole(String role);
    public User findUserByEmail(String email);

    public User findByEmail(String email);

    public List<User> findAll();

}
