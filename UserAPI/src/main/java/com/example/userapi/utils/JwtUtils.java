package com.example.userapi.utils;

import com.example.userapi.constants.SecurityConstants;
import com.example.userapi.entities.UserDetailImpl;
import io.jsonwebtoken.Jwts;
import io.jsonwebtoken.security.Keys;
import org.springframework.context.annotation.Configuration;
import org.springframework.security.core.Authentication;

import java.nio.charset.StandardCharsets;
import java.util.Date;

@Configuration
public class JwtUtils {

    private String jwtSecret;

    public String generateJwtToken(Authentication authentication) {
        UserDetailImpl userDetail = (UserDetailImpl) authentication.getPrincipal();
        return Jwts.builder().setSubject(userDetail.getUsername()).setIssuedAt(new Date()).setExpiration(new Date((new Date().getTime() + 7200))).signWith(Keys.hmacShaKeyFor(
                SecurityConstants.JWT_KEY.getBytes(StandardCharsets.UTF_8))).compact();

    }

    public boolean validateJwtToken(String token) {
        try {
            Jwts.parserBuilder().setSigningKey(Keys.hmacShaKeyFor(
                    SecurityConstants.JWT_KEY.getBytes(StandardCharsets.UTF_8))).build().parse(token);
            return true;
        }catch (Exception ex){
            return false;
        }
    }

    public String getUsernameFromJwt(String token) {
        return String.valueOf(Jwts.parserBuilder().setSigningKey(Keys.hmacShaKeyFor(
                SecurityConstants.JWT_KEY.getBytes(StandardCharsets.UTF_8))).build().parseClaimsJws(token).getBody().get("username"));

    }


}
