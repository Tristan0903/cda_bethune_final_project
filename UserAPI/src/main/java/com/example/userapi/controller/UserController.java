package com.example.userapi.controller;

import com.example.userapi.dto.request.RequestUserDTO;
import com.example.userapi.dto.response.ResponseUserDTO;
import com.example.userapi.entities.User;
import com.example.userapi.repository.UserRepository;
import com.example.userapi.service.UserService;

import com.example.userapi.utils.JwtUtils;
import org.modelmapper.ModelMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.authentication.AuthenticationManager;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequestMapping("/user")
@CrossOrigin(origins = {"*"}, methods = {RequestMethod.GET, RequestMethod.POST, RequestMethod.PUT})
public class UserController {

    @Autowired
    public UserService _userService;

    @Autowired
    private AuthenticationManager _authenticationManager;

    @Autowired
    private JwtUtils _jwtUtils;

    @Autowired
    private PasswordEncoder passwordEncoder;

    @Autowired
    ModelMapper _modelMapper;

    @PostMapping("/register")
    public ResponseEntity<ResponseUserDTO> register(@RequestBody User user){

    ResponseEntity response = null;
    try{
        String haspwd = passwordEncoder.encode(user.getPassword());
        user.setPassword(haspwd);
        user.setRole("USER");
        return ResponseEntity.ok(_userService.register(user.getEmail(), user.getPassword()));


    } catch (Exception e){
        response =  ResponseEntity.status(HttpStatus.INTERNAL_SERVER_ERROR).body("Une erreur est survenue" + e.getMessage());
    }
            return response;
    }

    @PostMapping("/login")
    public ResponseEntity<ResponseUserDTO> submitLogin(@RequestBody RequestUserDTO requestUserDTO) {
        try{
            Authentication authentication = _authenticationManager.authenticate(new UsernamePasswordAuthenticationToken(requestUserDTO.getEmail(), requestUserDTO.getPassword()));
            SecurityContextHolder.getContext().setAuthentication(authentication);
            String token = _jwtUtils.generateJwtToken(authentication);
            ResponseUserDTO user = _userService.findByEmail(requestUserDTO.getEmail());
            return ResponseEntity.ok(ResponseUserDTO.builder().id(user.getId()).email(user.getEmail()).role(user.getRole()).token(token).build());
        }catch (Exception e){
            return ResponseEntity.notFound().build();
        }
    }
    @GetMapping("")
    public ResponseEntity<List<ResponseUserDTO>> getAll(){
        return ResponseEntity.ok(_userService.findAllUsers());
    }

    @GetMapping("/id/{id}")
    public ResponseEntity<ResponseUserDTO> getById(@RequestParam int id) throws Exception {
        return ResponseEntity.ok(_userService.findById(id));
    }

    @GetMapping("/{email}")
    public ResponseEntity<ResponseUserDTO> getByEmail(@PathVariable String email){
        return ResponseEntity.ok(_userService.findByEmail(email));
    }

}
