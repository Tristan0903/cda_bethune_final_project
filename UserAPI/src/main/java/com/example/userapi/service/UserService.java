package com.example.userapi.service;

import com.example.userapi.dto.response.ResponseUserDTO;
import com.example.userapi.entities.User;
import com.example.userapi.repository.UserRepository;
import jakarta.servlet.http.HttpSession;
import org.modelmapper.ModelMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.List;


@Service
public class UserService {

    @Autowired
    public UserRepository _userRepository;

    @Autowired
    HttpSession _httpSession;

    @Autowired
    private ModelMapper _modelMapper;

    public ResponseUserDTO register(String email, String password){
        if(_userRepository.findUserByEmail(email) == null){
            User user = new User();
            user.setEmail(email);
            user.setPassword(password);
            user.setRole("USER");
            _userRepository.save(user);
            return _modelMapper.map(user, ResponseUserDTO.class);
        }
        return null;
    }

    public boolean login(String email){
        User user = _userRepository.findUserByEmail(email);
            _httpSession.setAttribute("isLogged", "OK");
            return true;
    }

   

    public List<ResponseUserDTO> findAllUsers(){
        List<ResponseUserDTO> responseUserDTOS = new ArrayList<>();
        if(_userRepository.findAll() != null){
            List<User> users = _userRepository.findAll();
            for (User u:users
                 ) {
                responseUserDTOS.add(_modelMapper.map(u, ResponseUserDTO.class));
            }
            return null;
        }
        return null;
    }

    public ResponseUserDTO findById(int id) throws Exception {
        if (_userRepository.findById(id).isPresent()){
            ResponseUserDTO responseUserDTO = _modelMapper.map(_userRepository.findById(id).get(), ResponseUserDTO.class);
        }
        throw new Exception();
    }

    public ResponseUserDTO findByEmail(String email){
        return _modelMapper.map(_userRepository.findUserByEmail(email), ResponseUserDTO.class);
    }

}
