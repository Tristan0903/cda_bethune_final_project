package com.example.userapi.dto.request;

import lombok.Data;

@Data
public class RequestUserDTO {
    private String email;

    private String password;

}
