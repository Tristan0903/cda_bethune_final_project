package org.example.services;


import org.example.dtos.response.ClaimDto;

import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;


public interface ClaimService {

    public ClaimDto createClaim(ClaimDto claimDto) throws Exception;
    public ClaimDto readClaim(int id);
    public List<ClaimDto> readClaims();
    public List<ClaimDto> getClaimByIdUser(int idUser) throws Exception;
    public ClaimDto updateStatusClaim(int id);

    public ClaimDto updateClaim(int id, String date, String category, String company, double price);



}
