package org.example.entity;

public enum Status {
    PENDING,
    VALIDATED,
    REFUND;
}
