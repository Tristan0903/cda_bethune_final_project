package org.example.entity;

import jakarta.persistence.*;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Entity
@Data
@AllArgsConstructor
@NoArgsConstructor
public class Proof {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private int id;
    private String url;
    @ManyToOne
    private Claim claim;

    public Proof(String url) {
        this.url = url;
    }

    @Override
    public String toString() {
        return "Proof{" +
                "id=" + id +
                ", url='" + url + '\'' +
                ", claim=" + claim +
                '}';
    }
}
