package org.example.entity;

import com.fasterxml.jackson.annotation.JsonIgnore;
import jakarta.persistence.*;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

@Entity
@Data
@AllArgsConstructor
@NoArgsConstructor
public class Claim {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private int id;
    private int idUser;
    private String date;
    private String category;
    private String company;
    private double price;

    private Status status = Status.PENDING;
    @JsonIgnore
    @OneToMany(mappedBy = "claim",cascade = CascadeType.ALL,fetch = FetchType.LAZY)
    private List<Proof> proofs;


}
